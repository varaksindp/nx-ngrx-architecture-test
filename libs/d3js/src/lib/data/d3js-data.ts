export const patchObj = {
  G: "M0 0 C50 50 50 100 0 100 C-50 100 -50 50 0 0",
  PG: "M-35 0 C-25 25 25 25 35 0 C50 25 25 75 0 100 C-25 75 -50 25 -35 0",
  R: "M0 0 C50 25 50 75 0 100 C-50 75 -50 25 0 0"
}

export const Colors = {
  Action: "#ffc8f0",
  Comedy: "#cbf2bd",
  Animation: "#afe9ff",
  Drama: "#ffb09e",
  Other: "#FFF2B4",
}

export const movies = [
  {
    title: "The Legend of Tarzan",
    released: "2016-06-30T22:00Z",
    genres: ["Action", "Adventure"],
    rating: 7.6,
    votes: 541,
    rated: "G"
  },
  {
    title: "Independence Day: Resurgence",
    released: "2016-06-23T22:00Z",
    genres: ["Action", "Adventure", "Sci-Fi"],
    rating: 5.9,
    votes: 7058,
    rated: "G"
  },
  {
    title: "Central Intelligence",
    released: "2016-06-16T22:00Z",
    genres: ["Comedy", "Crime"],
    rating: 7,
    votes: 4663,
    rated: "G",
  },
  {
    title: "Straight Outta Compton",
    released: "2015-08-13T22:00Z",
    genres: ["Biography", "Crime", "Drama"],
    rating: 8,
    votes: 110326,
    rated: "R"
  },
  {
    title: "Minions",
    released: "2015-07-09T22:00Z",
    genres: ["Animation", "Action", "Comedy"],
    rating: 6.4,
    votes: 135756,
    rated: "PG"
  }
]
