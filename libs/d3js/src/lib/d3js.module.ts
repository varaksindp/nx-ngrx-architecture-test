import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { D3jsComponent } from './d3js.component';
import { RouterModule } from '@angular/router';
import {MatButtonModule} from "@angular/material/button";

@NgModule({
  imports: [
    CommonModule,
    MatButtonModule,
    RouterModule.forChild([
      {
        path: '',
        component: D3jsComponent,
      },
    ]),
  ],
  declarations: [D3jsComponent],
})
export class D3jsModule {}
