import {ChangeDetectionStrategy, Component, OnInit} from "@angular/core";
import {select, selectAll} from 'd3-selection';
import {
  scaleLinear,
  scaleTime,
  scaleOrdinal,
} from 'd3-scale';
import * as d3 from 'd3';
import {Colors, movies, patchObj} from "./data/d3js-data";
import {colors} from "@angular/cli/utilities/color";

@Component({
  selector: 'ngrx-test-d3js',
  templateUrl: './d3js.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class D3jsComponent implements OnInit {

  // CHARTS
  svg;
  rect;
  allRect
  rectWidth = 50;
  svgHeight = 100;
  barData = [7, 14, 51];
  colorScale;

  //FLOWERS
  svg2;
  svg2Paths;

  ngOnInit() {
    // CHARTS
    this.svg = select('#container');
    this.rect = this.svg.select('rect');

    this.colorScale = d3.scaleOrdinal()
      .domain(['Action', 'Comedy', 'Adventure', 'Drama'])
      .range(['#ffc8f0', '#cbf2bd', '#afe9ff', '#ffb09e'])
      .unknown(Colors.Other)

    const newMovies = movies.map((x, index) => {
      return {
        color: this.colorScale(x.genres[0]),
        title: x.title,
        translate: this.calculateGridPosition(index)
      }
    })

    console.log(newMovies, 'newMovies');

    this.allRect = this.svg.selectAll('rect')
      .data(this.barData).enter().append('rect')
      .attr('x', (d, i) => i * this.rectWidth)
      .attr('y', d => this.svgHeight - d)
      .attr('height', d => d)
      .attr('width', this.rectWidth)
      .attr('stroke-width', 3)
      .attr('stroke', 'plum')
      .attr('fill', 'pink')


    // FLOWERS
    this.svg2 = select('#container-2');
    this.svg2Paths = this.svg2.selectAll('path')
      .data(movies).enter().append('path')
      .attr('transform', (d, i) => `translate(${this.calculateGridPosition(i)})`)
      .attr('d', d => patchObj[d.rated])
      .attr('fill', d => Colors[d.genres[0] || Colors.Other])
  }

  // Position in a grid
  calculateGridPosition(i) {
    return [(i % 3 + 0.5) * 150, (Math.floor(i / 3) + 0.5) * 150]
  }

  // Changing data in a chart
  changeData() {
    // Array of 1-5 entetiens from 1 to 100
    this.barData = Array.from({length: this.getRandomInt(1,6)}, () => Math.floor(Math.random() * 101));

    console.log(this.barData, 'randomlyGenArray')

    this.svg.selectAll('rect')
      .data(this.barData, d => d)
      .join(
        enter => {
          return enter.append('rect')
          // attributes to transition FROM
            .attr('x', (d,i) => i * this.rectWidth)
            .attr('fill', 'pink')
            .attr('stroke', 'plum')
            .attr('stroke-width', 2)
            .attr('height', 0)
            .attr('y', this.svgHeight)
        },
        update => update,
        exit => {
          exit.transition(1000)
          // everything after here is transition TO
            .attr('height', 0)
            .attr('y', this.svgHeight)
        }
      ) // enter + update selection
      .attr('width', this.rectWidth)
      .transition(1000)
      // attributes to transition TO
      .attr('x', (d,i) => i * this.rectWidth)
      .attr('y', d => this.svgHeight-d)
      .attr('height', d => d)
  }


 getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min)) + min; //Максимум не включается, минимум включается
  }
}
