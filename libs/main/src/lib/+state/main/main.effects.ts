import {Injectable} from '@angular/core';
import {Actions, createEffect, ofType} from '@ngrx/effects';

import * as MainActions from './main.actions';
import {catchError, concatMap, map, of} from "rxjs";
import {BooksService} from "../../services/books.service";

@Injectable()
export class MainEffects {
  constructor(private readonly actions$: Actions, private booksService: BooksService,) {}

  loadBooks$ = createEffect(() =>
    this.actions$.pipe(
      ofType(MainActions.loadBooks),
      concatMap((action) => {
        return this.booksService.getBooks(action.searchTerm).pipe(
          map((x: any) => MainActions.loadBooksSuccess({
            books: x.items,
          }))
        )
      }),
      catchError((x) => {
        return of(MainActions.loadBooksFailure({error: x}))
      })
    )
  )
}
