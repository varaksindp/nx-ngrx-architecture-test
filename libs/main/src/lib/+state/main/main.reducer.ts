import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';
import { createReducer, on, Action } from '@ngrx/store';

import * as MainActions from './main.actions';
import { Books } from '../../interfaces/books';
import { loadBooks } from './main.actions';

export const MAIN_FEATURE_KEY = 'main';

export interface MainState {
  books: Books[]; // which Main record has been selected
  loaded: boolean; // has the Main list been loaded
  error: string; // last known error (if any)
}

const initialState = {
  books: [] as Books[],
  loaded: true,
  error: ''
};

const mainReducer = createReducer(
  initialState,
  on(MainActions.init, (state) => ({ ...state, loaded: true, error: '' })),
  on(MainActions.loadBooks, (state, payload) => ({
    ...state,
    loaded: false,
    error: '',
  })),
  on(MainActions.loadBooksSuccess, (state, payload) => ({
    books: payload.books,
    loaded: true,
    error: '',
  })),
  on(MainActions.loadBooksFailure, (state, payload) => ({
    ...state,
    loaded: true,
    error: payload.error,
  }))
);

export function reducer(state: MainState, action: Action) {
  return mainReducer(state, action);
}
