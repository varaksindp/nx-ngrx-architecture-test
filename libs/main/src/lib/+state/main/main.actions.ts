import { createAction, props } from '@ngrx/store';
import { Books } from '../../interfaces/books';

export const init = createAction('[Main Page] Init');

export const loadBooks = createAction(
  '[Main/API] Load books',
  props<{ searchTerm: string; books?: Books[] }>()
);

export const loadBooksSuccess = createAction(
  '[Main/API] Load Main Success',
  props<{ books: Books[] }>()
);

export const loadBooksFailure = createAction(
  '[Main/API] Load Main Failure',
  props<{ error: any }>()
);
