import {MainState} from "@ngrx-test/main";

export interface AppState {
  main: MainState
}
