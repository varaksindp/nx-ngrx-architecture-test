import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {BooksService} from './services/books.service';
import {FormBuilder, Validators,} from '@angular/forms';
import {Observable} from 'rxjs';
import {Books} from './interfaces/books';
import {Store} from '@ngrx/store';
import * as MainActions from './+state/main/main.actions';
import {AppState} from "./+state/main";

@Component({
  selector: 'ngrx-test-main',
  templateUrl: './main.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MainComponent implements OnInit {
  constructor(
    private booksService: BooksService,
    private formBuilder: FormBuilder,
    private store: Store<AppState>
  ) {}

  form = this.formBuilder.group({
    bookName: ['', Validators.required],
  });
  books!: Observable<Books[]> ;

  ngOnInit() {
    this.store.dispatch(MainActions.init());
  }

  confirm() {
    this.store.dispatch(
      MainActions.loadBooks({searchTerm: this.form.controls['bookName'].value})
    );

    this.books = this.store.select((state) => state.main.books)
  }
}
