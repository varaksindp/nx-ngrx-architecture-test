import { Injectable } from '@angular/core';
import { books } from '../environments/books';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class BooksService {
  private url = `${books.baseUrl}books/v1/volumes`;

  constructor(private http: HttpClient) {}

  getBooks(name: string) {
    return this.http.get<any>(
      `${this.url}?q=${name}:keyes&key=${books.userId}`
    );
  }
}
