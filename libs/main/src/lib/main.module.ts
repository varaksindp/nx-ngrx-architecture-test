import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './components/header/header.component';
import { MainComponent } from './main.component';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import * as fromMain from './+state/main/main.reducer';
import { MainEffects } from './+state/main/main.effects';
import { RouterModule } from '@angular/router';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatCardModule } from '@angular/material/card';
import { BooksService } from './services/books.service';
import { MatInputModule } from '@angular/material/input';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';

@NgModule({
  imports: [
    CommonModule,
    MatToolbarModule,
    MatCardModule,
    MatInputModule,
    MatButtonModule,
    FormsModule,
    ReactiveFormsModule,
    StoreModule.forFeature(fromMain.MAIN_FEATURE_KEY, fromMain.reducer),
    EffectsModule.forFeature([MainEffects]),
    RouterModule.forChild([
      {
        path: '',
        component: MainComponent,
      },
    ]),
  ],
  declarations: [HeaderComponent, MainComponent],
  exports: [MainComponent],
  providers: [BooksService],
})
export class MainModule {}
