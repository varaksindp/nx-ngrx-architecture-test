export interface Books {
  kind: string;
  id: string;
  etag: string;
  selfLink: string;
  volumeInfo: volumeInfo;
}

export interface volumeInfo {
  title: string;
  publishedDate: string;
  language: string;
  infoLink: string;
  imageLinks: ImageLimks;
}

export interface ImageLimks {
  smallThumbnail: string;
  thumbnail: string;
}
