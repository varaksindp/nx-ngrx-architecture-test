import { Component } from '@angular/core';

@Component({
  selector: 'ngrx-test-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'ngrx-test';
}
