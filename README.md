# NgrxTest

<h3>
This project was generated using [Nx] (https://nx.dev).
This project aproves a concept of Angular + Nx + NgRX application.
</h3>

<br/>
<br/>
To generate new NgRX logic for a module used a command:
<br/>
nx g @nrwl/angular:ngrx main --module=libs/main/src/lib/main.module.ts --directory +state/main --defaults
<br/>
[NgRX] (https://nx.dev/guides/misc-ngrx)
<br/>
<br/>
To run app, execute:
<br/>
npm install
<br/>
nx serve
<br/>
<br/>
Developed by
Varaksin Denis
